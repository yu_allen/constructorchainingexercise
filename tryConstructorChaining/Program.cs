﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tryConstructorChaining
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 範例1
            //第一個建構式沒有鏈結第二個建構式，所以結果只會顯示"Hello"後面是空的
            showText _showText_A = new showText();
            //第二個建構式有鏈結第一個建構式，所以結果會有"Hello"也會有"Sam"
            showText _showText_B = new showText("Sam");
            //印出_showText_A結果
            Console.Write(_showText_A.TextOne);
            Console.WriteLine(_showText_A.TextTwo);

            //印出_showText_B結果
            Console.Write(_showText_B.TextOne);
            Console.WriteLine(_showText_B.TextTwo);
            #endregion

            #region 範例2
            //第二個建構式鏈結了第一個建構式，所以初始化的如果是第二個建構式會同時有NumOne及NumTwo的值
            //第三個建構式又鏈結了第二個，所以除了第三個建構式自己初始化時設定的值以外同時會有NumOne及NumTwo的值
            showNumber _showNumber_Obj = new showNumber("英文成績單 : ");
            Console.WriteLine(_showNumber_Obj.SubjectName);
            Console.WriteLine("項目1 : " + _showNumber_Obj.NumOne);
            Console.WriteLine("項目2 : " + _showNumber_Obj.NumTwo);
            Console.WriteLine("總計 : " + _showNumber_Obj.NumSum);
            #endregion

            Console.Read();
        }
    }

    class showText {
        public string TextOne { set; get; }
        public string TextTwo { set; get; }
        public showText()
        {
            TextOne = "Hello";
        }
        public showText(string UserName):this()
        {
            TextTwo = UserName;
        }
    }

    class showNumber
    {
        public int NumOne { set; get; }
        public int NumTwo { set; get; }
        public int NumSum { set; get; }
        public string SubjectName { set; get; }
        public showNumber()
        {
            NumOne = 3;
        }
        public showNumber(int Num) : this()
        {
            NumTwo = Num;
        }

        public showNumber(string _SubjectName) :this(9)
        {
            NumSum = NumOne + NumTwo;
            SubjectName = _SubjectName;
        }
    }
}
